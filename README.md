# Instruction to run the code

Install pip: https://pip.pypa.io/en/stable/installing/

Install virtualenv: https://virtualenv.pypa.io/en/stable/installation/

Activate virtualenv:

`virtualenv .env`

`source .env/bin/activate`

Install required dependencies:

`pip install -r requirements.txt`

Run the scrapper

`scrapy runspider insee.py`
