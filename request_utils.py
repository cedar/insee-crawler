import json

import requests

from log_utils import logger


def post(url, payload, timeout=15):
    ''' Perform a HTTP POST request

    Args:
      url (str):
      payload (dict):
      timeout (int): default value is 15 seconds

    Returns:
      a dictionary of the response (JSON format), or None if there's error when requesting the data
    '''
    headers = {'Content-Type': 'application/json'}

    try:
        r = requests.post(url, data=json.dumps(payload),
                          headers=headers, timeout=timeout)
        if r.status_code == 200:
            return json.loads(r.text)
    except Exception as e:
        logger.error('An exception occurs: {}'.format(e))
        return None
    else:
        return None
