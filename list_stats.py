#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
from config import (LIST_PUBLICATION_FILE, LIST_STATS_FILE, NUM_RETRY,
                    RANDOM_WAITING_TIME)
from random import randint

import utils
from log_utils import logger
from request_utils import post


def get_payload(start_id, category_id):
    return {
        "q": "*:*",
        "defType": "edismax",
        "start": start_id,
        "sortFields": [
            {
                "field": "dateDiffusion",
                "order": "desc"
            }
        ],
        "filters": [
            {
                "field": "categorieId",
                "tag": "tagCategorieId",
                "values": [
                    category_id
                ]
            },
            {
                "field": "rubrique",
                "tag": "tagRubrique",
                "values": [
                    "statistiques"
                ]
            },
            {
                "field": "diffusion",
                "values": [
                    True
                ]
            }
        ],
        "rows": 10,
        "facetsQuery": []
    }


def get_all_categories_payload():
    return {"q": "*:*", "defType": "edismax", "start": 0, "sortFields": [{"field": "dateDiffusion", "order": "desc"}], "filters": [{"field": "rubrique", "tag": "tagRubrique", "values": ["statistiques"]}, {"field": "diffusion", "values": [True]}], "rows": 10, "facetsQuery": []}


def is_dev_mode():
    return '--dev' in sys.argv


def collect_stats_info(category_id, total_pages, file_name):
    logger.info('Collect IDs from category {}'.format(category_id))
    stats_file = open(file_name, 'a')

    url = 'https://www.insee.fr/fr/solr/consultation'

    start_ids = range(0, int(total_pages / 10) +
                      1) if not is_dev_mode() else range(0, 1)
    for start_id in start_ids:
        logger.info('Progress {}/{}'.format(start_id * 10, total_pages))

        payload = get_payload(start_id * 10, category_id)

        count_retry = 0
        while count_retry < NUM_RETRY:
            if count_retry > 0:
                logger.info('Retry url {}'.format(url))

            response = post(url, payload)
            if response:
                list_documents = response['documents']

                for document in list_documents:
                    normalized_title = utils.clean_html(document['titre'])
                    stats_file.write('{}\t{}\n'.format(
                        document['id'], normalized_title))

                time.sleep(float(randint(0, RANDOM_WAITING_TIME)) / 1000)
                break
            else:
                count_retry += 1

    stats_file.close()


def collect_insee_ids():
    ''' Collect insee IDs and page title from https://www.insee.fr/fr/statistiques
    and write the results to `config.LIST_STATS_FILE` and `config.LIST_PUBLICATION_FILE`
    '''
    response = post("https://www.insee.fr/fr/solr/consultation?q=*:*",
                    get_all_categories_payload())
    list_documents = response['facetsField']
    for document in list_documents:
        if document['name'] == 'categorieId':
            num_pages = document['data']
            # Bases de données
            collect_stats_info(3, num_pages['3'], LIST_STATS_FILE)
            # Chiffres-clés
            collect_stats_info(4, num_pages['4'], LIST_STATS_FILE)
            # Chiffres détaillés
            collect_stats_info(5, num_pages['5'], LIST_STATS_FILE)
            # Publications grand public
            collect_stats_info(12, num_pages['12'], LIST_PUBLICATION_FILE)


if __name__ == '__main__':
    collect_insee_ids()
