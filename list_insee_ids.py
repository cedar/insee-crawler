'''
Generate list of all INSEE ids from the collected dataset
'''

import csv
import sys
from list_stats import LIST_PUBLICATION_FILE, LIST_STATS_FILE

#INSEE_IDS_FILE = 'insee_ids.txt'
INSEE_IDS_FILE = sys.argv[1]


def get_list_insee_ids(tsv_file):
    with open(tsv_file) as f:
        reader = csv.reader(f, delimiter='\t')
        list_ids = list()
        for row in reader:
            list_ids.append(row[0])
        return list_ids


insee_ids = set(get_list_insee_ids(LIST_PUBLICATION_FILE) +
                get_list_insee_ids(LIST_STATS_FILE))

with open(INSEE_IDS_FILE, 'w+') as f:
    for _id in insee_ids:
        f.write('{}\n'.format(_id))
