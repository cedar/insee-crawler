import config
import logging
import os


def get_log(file_name, overwrite=False):
    """
    Logger with date time formated
    """
    if overwrite and os.path.exists(file_name):
        os.remove(file_name)

    log_name = file_name[:file_name.index('.')]
    logger = logging.getLogger(log_name)

    logging.basicConfig(level=logging.INFO)
    handler = logging.FileHandler(file_name)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


logger = get_log(config.LOG_FILE)
