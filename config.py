OUTPUT_FOLDER = '/data/tcao/insee/'
LIST_DOWNLOADED_FILES = 'downloaded_files.csv'
ALL_INSEE_IDS_FILE = 'insee_ids.txt'

EXCEL_EXTRACTOR_INPUT = 'xls_extractor_input.csv'
HTML_EXTRACTOR_INPUT = 'html_extractor_input.csv'

LOG_FILE = 'insee_scrapper.log'


LIST_STATS_FILE = 'stats.tsv'
LIST_PUBLICATION_FILE = 'pubs.tsv'

NUM_RETRY = 3

RANDOM_WAITING_TIME = 1000  # miliseconds
