import csv
from config import (EXCEL_EXTRACTOR_INPUT, HTML_EXTRACTOR_INPUT,
                    LIST_DOWNLOADED_FILES)


download_files = open(LIST_DOWNLOADED_FILES)
next(download_files)
csv_reader = csv.reader(download_files, delimiter=',')

excel_extractor_input_writer = csv.writer(open(EXCEL_EXTRACTOR_INPUT, 'w+'))
excel_extractor_input_writer.writerow(
    'downloaded_date,url,file,description,published_date'.split(',')
)

html_extractor_input_writer = csv.writer(open(HTML_EXTRACTOR_INPUT, 'w+'))
html_extractor_input_writer.writerow(
    'downloaded_date,url,file,description,published_date'.split(',')
)

for row in csv_reader:
    downloaded_date, url, file_name = row[:3]
    description = ' '.join(row[3:-1])
    published_date = row[-1]
    if '.xls' in file_name or '.zip' in file_name:
        excel_extractor_input_writer.writerow(row)
    elif '.html' in file_name:
        html_extractor_input_writer.writerow(row)
