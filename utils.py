import os
from bs4 import BeautifulSoup
from time import gmtime, strftime


def create_folder(new_path):
    if not os.path.exists(new_path):
        os.makedirs(new_path)


def clean_empty_spaces(text):
    return ' '.join(text.split()).replace('\t', ' ').replace('\n', ' ')


def clean_html(html):
    soup = BeautifulSoup(html, "html.parser")
    return clean_empty_spaces(soup.get_text().strip())


def get_current_time():
    return strftime("%Y-%m-%d %H:%M:%S", gmtime())
