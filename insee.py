import codecs
import logging
import sys
import urllib.request
from config import (ALL_INSEE_IDS_FILE, LIST_DOWNLOADED_FILES,
                    LIST_PUBLICATION_FILE, LIST_STATS_FILE, OUTPUT_FOLDER)

import scrapy

import utils
from log_utils import logger

INSEE_DOMAIN = 'http://www.insee.fr'
BASE_STATS_LINK = 'https://www.insee.fr/fr/statistiques/'


# metadata of downloaded files
downloaded_files_csv = codecs.open(LIST_DOWNLOADED_FILES, 'w+', 'utf-8')
downloaded_files_csv.write(
    'downloaded_date,url,file,description,published_date\n')


def update_downloaded_files(url, file_name, description, published_date):
    downloaded_files_csv.write('%s,%s,%s,"%s",%s\n' % (
        utils.get_current_time(),
        url,
        file_name,
        description,
        published_date
    ))


def download_file(download_link, insee_id, description, published_date):
    file_name = download_link[download_link.rindex('/') + 1:]
    logger.info('download_link {}'.format(download_link))
    logger.info('[File name] ' + file_name)
    urllib.request.urlretrieve(
        download_link, OUTPUT_FOLDER + insee_id + '/' + file_name)

    update_downloaded_files(download_link, file_name,
                            description, published_date)


class InseeSpider(scrapy.Spider):
    name = 'insee'
    counter = 0
    # allowed_domains = ["insee.com"]

    custom_settings = {
        'AUTOTHROTTLE_ENABLED': True,
        'HTTPCACHE_ENABLED': True
    }

    def process_tsv_file(self, tsv_file, existing_insee_ids):
        with open(tsv_file) as f:
            lines = f.readlines()
            for line in lines:
                insee_id = line.split('\t')[0]
                if insee_id not in existing_insee_ids:
                    logger.info('Processing insee_id {}'.format(insee_id))
                    yield scrapy.Request(BASE_STATS_LINK + insee_id, self.parse)

    def start_requests(self):
        # insee_id = '3141092'
        # yield scrapy.Request(BASE_STATS_LINK + insee_id, self.parse)

        with open(ALL_INSEE_IDS_FILE) as f:
            existing_insee_ids = f.read().splitlines()

        with open(LIST_STATS_FILE) as f:
            lines = f.readlines()
            for line in lines:
                insee_id = line.split('\t')[0]
                # TODO duplicated code
                # TODO handle existing IDs when updating the scrapper daily
                # if insee_id not in existing_insee_ids:
                yield scrapy.Request(BASE_STATS_LINK + insee_id, self.parse)

        with open(LIST_PUBLICATION_FILE) as f:
            lines = f.readlines()
            for line in lines:
                insee_id = line.split('\t')[0]
                # if insee_id not in existing_insee_ids:
                yield scrapy.Request(BASE_STATS_LINK + insee_id, self.parse)

    def parse(self, response):
        if not response.css('.lien-produit').extract():
            self.parse_dataset(response)
        else:
            for link in response.css('.lien-produit a::attr(href)').extract():
                yield scrapy.Request(INSEE_DOMAIN + link, self.parse_dataset)

    def parse_dataset(self, response):
        current_url = response.url
        self.counter += 1
        logger.info('PROCESSING URL #' + str(self.counter))
        logger.info('Parsing url ' + current_url)

        if '?' in current_url:
            insee_id = current_url[current_url.rindex(
                '/') + 1: current_url.index('?')]
        else:
            insee_id = current_url[current_url.rindex('/') + 1:]

        utils.create_folder(OUTPUT_FOLDER + insee_id)

        published_date = response.css('.date-diffusion').extract_first()
        if not published_date:
            published_date = ''
        else:
            published_date = utils.clean_html(published_date)
        logger.info('published_date, {}, {}'.format(insee_id, published_date))

        complementary_document = response.css('.donnees-telechargeables')
        if complementary_document:
            for download in complementary_document.css('a'):
                link = download.css('::attr(href)').extract_first()
                if '.xls' in link:
                    download_link = INSEE_DOMAIN + link
                    logger.info(
                        'complementary_document {}'.format(download_link))
                    download_file(download_link, insee_id, '', published_date)

        for figure in response.css('#consulter .bloc.figure'):
            utils.create_folder(OUTPUT_FOLDER + insee_id)

            html_table = figure.extract()

            file_name = figure.css(
                'figure::attr(id)').extract_first() + '.html'

            description = utils.clean_html(
                figure.css('.titre-figure').extract_first())
            sub_title_css = figure.css('.titre-figure .sous-titre')
            if sub_title_css:
                sub_title = utils.clean_html(
                    sub_title_css.extract_first()) or ''
                if sub_title:
                    description = description.replace(
                        sub_title, ' ' + sub_title)
            logger.info('[Table name] ' + description)
            with open(OUTPUT_FOLDER + insee_id + '/' + file_name, 'w') as f:
                f.write(html_table)

            update_downloaded_files(
                current_url, file_name, description, published_date)

        for block in response.css('#consulter .bloc.fichiers'):
            for document in block.css('.fichier'):
                file_format = document.css(
                    '.fichier-format::text').extract_first()
                if 'xls' in file_format or 'zip' in file_format:
                    utils.create_folder(OUTPUT_FOLDER + insee_id)

                    href = document.css('a::attr("href")').extract_first()

                    name = block.css(
                        '.nom-fichier::text').extract_first() or ''
                    title = block.css(
                        '.titre-fichier::text').extract_first() or ''
                    description = name + title
                    description = utils.clean_html(description)

                    if description != 'Les tableaux en Excel' and description != 'Les tableaux en Beyond':
                        download_link = response.urljoin(href)
                        download_file(download_link, insee_id,
                                      description, published_date)
